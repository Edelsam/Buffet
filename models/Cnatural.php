<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Cnatural".
 *
 * @property string $Nombre1
 * @property string $Nombre2
 */
class Cnatural extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Cnatural';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre1', 'Nombre2'], 'required'],
            [['Nombre1', 'Nombre2'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Nombre1' => 'Nombre1',
            'Nombre2' => 'Nombre2',
        ];
    }
}
